import sqlite3
conn = sqlite3.connect('../../../http_server_env/water.db')
print ("Opened database successfully")

result=conn.execute("CREATE TABLE `device_list` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `location` varchar(25) NOT NULL DEFAULT 'no', `mac_address` varchar(20) NOT NULL, `check_point` timestamp NULL DEFAULT (datetime('now','localtime')),  `setting` varchar(300) DEFAULT NULL,  `rotation` int NOT NULL DEFAULT '0',  `device_type` int NOT NULL DEFAULT '0',`flashlight` int NOT NULL DEFAULT '1',`duty_cycle` int NOT NULL DEFAULT '125');")
  
print ("Table created successfully")


result=conn.execute("CREATE TABLE `value_list` (  `index` INTEGER PRIMARY KEY AUTOINCREMENT,  `location` varchar(25) NOT NULL,  `value` int NOT NULL,  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  `value_type` int NOT NULL DEFAULT '0',  `user_entry` int NOT NULL DEFAULT '0');")
  
print ("Table created successfully")



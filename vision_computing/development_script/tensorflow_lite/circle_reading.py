import numpy as np
import pytesseract 
import argparse
import imutils
import cv2
import os
import tflite_runtime.interpreter as tflite
import xlsxwriter
import time

circle_dir="../circle_images/"
single_digit_dir="../single_digit_images/"
img_height = 28
img_width = 28

def decode_img(img):
  #img= tf.io.read_file(img)
  #img = tf.image.decode_png(img, channels=3) 
  img = tf.image.resize(img, [img_height, img_width])
  img = (np.expand_dims(img,0))
  return img

def image_convert(img):
    img=cv2.resize(img, (img_height,img_width), interpolation = cv2.INTER_AREA)
    img = (np.expand_dims(img,0))
    return img.astype(np.float32)

interpreter = tflite.Interpreter('../final_model_lite_own.h5')
interpreter.allocate_tensors()
# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

workbook = xlsxwriter.Workbook('tf_output.xlsx')
worksheet = workbook.add_worksheet()
row = 1

cnt_space=40
cnt_x=90
cnt_y=265
cnt_w=30
cnt_h=60
image_number=0
paths =os.listdir(circle_dir)
paths=sorted(paths)
number_of_success=0
index=0
start = time.time()

for file in paths:
    index +=1
    image = cv2.imread(os.path.join(circle_dir,file))
    #image = cv2.rotate(image, cv2.ROTATE_180)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)
    height, width = image.shape[:2]
    wide = cv2.Canny(blurred, 10, 200)
    cnts = cv2.findContours(wide, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    whole_number=""
    for i in range(6):
        image_number+=1
        #cv2.rectangle(image,(cnt_x+i*cnt_space,cnt_y), (cnt_x+i*cnt_space+cnt_w,cnt_y+cnt_h), (0,255,0), 1) 
        #cv2.imwrite(os.path.join(single_digit_dir,str(image_number)+".png"),gray[cnt_y:cnt_y+cnt_h,cnt_x+i*cnt_space:cnt_x+i*cnt_space+cnt_w])
        img=cv2.cvtColor(gray[cnt_y:cnt_y+cnt_h,cnt_x+i*cnt_space:cnt_x+i*cnt_space+cnt_w],cv2.COLOR_GRAY2RGB)
        print(img.shape)
        img=image_convert(img)
        print(img.shape)
        interpreter.set_tensor(input_details[0]['index'], img)
        interpreter.invoke()
        digit = interpreter.get_tensor(output_details[0]['index'])
        whole_number += class_names[np.argmax(digit[0], axis=0)]
        worksheet.write(row, 0, file)
        worksheet.write(row, 1, whole_number)
    print("-------------")
    print(os.path.join(circle_dir,file))
    print(whole_number)
    print("prcoess: ",index/len(paths))
    row += 1
end = time.time()
workbook.close()

print("the total time:  ")
print(end - start)

#include <Arduino.h>
#include <WiFi.h>
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"

//------------------------------------------------------------------Constants and variables------------------------------------------------------------------------------------------------------------------------------------------
//Relevant information for wi-fi connection and server information
//The ssid and password will have to be filled in it
const char* ssid = "";
const char* password = "";
String Host = "www.feng.family";   
String Path = "/api/device_input";
const int Port = 80;
WiFiClient wificlient;

//Relevant variable for DeepSleep timer
#define conversionFactor 1000000 //deep_sleep_timer takes micros seconds, this converts it to seconds for my sanity
#define minuteConversion 30 //conversion factor from minutes to seconds
#define sleepTime 10 //timer for sleep in minutes

//Assigning pins for microphone ADC and photoresistor GPIO
const int ADCPin= 34; //pin connected to microhone output
const int lightPin = 33;  //pin connected to 
const int lightThreshold = 80; //Threshold value for photoresistor and light being on.

RTC_DATA_ATTR String storedIssueBuffer[] = {"5", "5", "5", "5", "5", "5", "5", "5", "5", "5", "5", "5"};  //RTC data type to store information that needs to be sent in case of wi-fi failure
String issueState = "5";
const int bufferLoop = 12;  //Buffer has 12 stored 'strings'

//variables to store and check input from the microphone ADC.
int tempVal = 0;
int ADC_VALUE = 0;
int ADC_MAX = 0;
int ADC_MIN = 4095;
int ADC_SUM = 0;
int ADC_AVG = 0;
int AvgDiff = 0;
int MaxDiff = 0;
int MinDiff = 0;
int ADCSpreadMax = 0;
int ADCSpreadMin = 0;
int ADCSpreadTotal = 0;
int plus10, plus20, plus30, plus40, plus50, plus60, plus70, plus80, plus90, plus100, plus110, plus120, plus130, plus140, plus150, plus160, plus170, plus180, plus190, plus200,plus210, plus220, plus230, plus240, plus250 = 0;
int plus260, plus270, plus280, plus290, plus300, plus310, plus320, plus330, plus340, plus350, plus360, plus370, plus380, plus390, plus400, plus410, plus420, plus430, plus440, plus450 = 0; 
int diffBuffer = 0;
const int numReadingsBase = 1000;  //number of readings to get '0' value of microphone that will be compared against
const int numReadings = 60000;  //total number of readings
const int timeDelay = 2; //takes a reading every tenth of a second
int noiselessAVG = 0; //The '0' reading of the microphone to compare against
int posVal = 0; //Counts the number of data points above the '0' reading

//------------------------------------------------------------------Functions------------------------------------------------------------------------------------------------------------------------------------------
//Function to connect to Wi-fi
void connectWiFi() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); 
  
  String wifi_mac=WiFi.macAddress();
  Serial.println(wifi_mac);

  WiFi.mode(WIFI_STA);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);  
  //attemp to connect the wifi at least 10 times
  for(int i=0;i<10;i++) {
    WiFi.begin(ssid, password);  
    Serial.print(".");
    if(WiFi.status() == WL_CONNECTED)
    {
       Serial.print("Connected YAY!");
       break;
    }
    delay(10000);
  }

  Serial.println();
  Serial.print("ESP32 IP Address: ");
  Serial.println(WiFi.localIP());
}

//Fucntion to send data to a website over Wi-Fi, recieved by Von and then modified to have the relevent data
//Takes the value 'a' which will signifiy if there is an issue in the room and relay that information.
void sendIssueState(String a) {

  //form information
  String head_0 = "--esp32boundary\r\nContent-Disposition: form-data; name=\"value\"; \r\nContent-Type: text/plain\r\n\r\n";
  /*
   * 
   * assigned value for string format to content_0, in this case 0 since everything is fine
   * 
   */
  String content_0= a;
  String tail_0 = "\r\n--esp32boundary\r\n";

  
  String head_1 = "--esp32boundary\r\nContent-Disposition: form-data; name=\"mac_address\"; \r\nContent-Type: text/plain\r\n\r\n";
  String content_1=WiFi.macAddress();
  String tail_1 = "\r\n--esp32boundary\r\n";

  //device_type=1 for microphone
  String head_2 = "--esp32boundary\r\nContent-Disposition: form-data; name=\"device_type\"; \r\nContent-Type: text/plain\r\n\r\n";
  String content_2="1";  
  String tail_2 = "\r\n--esp32boundary--\r\n";

  //message length
  uint32_t totalLen = head_0.length()+content_0.length()+ tail_0.length()+head_1.length()+content_1.length() + tail_1.length()+head_2.length()+content_2.length() + tail_2.length();
  

if (wificlient.connect(Host.c_str(), Port))
  {
    Serial.println("wifi Connection successful!");
    wificlient.println("POST " + Path + " HTTP/1.1");
    wificlient.println("Host: " + Host);
    wificlient.println("Content-Length: " + String(totalLen));
    wificlient.println("Content-Type: multipart/form-data; boundary=esp32boundary");
    wificlient.println();


    //send value
    wificlient.print(head_0);
    wificlient.print(content_0);
    wificlient.print(tail_0);

    //send mac address
    wificlient.print(head_1);
    wificlient.print(content_1);
    wificlient.print(tail_1);

    
    //send device type
    wificlient.print(head_2);
    wificlient.print(content_2);
    wificlient.print(tail_2);


    String getAll;
    String getBody;
    int timoutTimer = 10000;
    long startTimer = millis();
    boolean state = false;  
    while ((startTimer + timoutTimer) > millis()) {
      Serial.print(".");
      delay(100);      
      while (wificlient.available()) {
        char c = wificlient.read();
        if (c == '\n') {
          if (getAll.length()==0) { state=true; }
          getAll = "";
        }
        else if (c != '\r') { getAll += String(c); }
        if (state==true) { getBody += String(c); }
        startTimer = millis();
      }
      if (getBody.length()>0) { break; }
    }
    Serial.println();
    wificlient.stop();
    Serial.println(getBody);     
  }
  else
  {
    Serial.println("no connection available");
  }  
}


//Reads and returns the state on Pin33.
//Return: high is ON, low is Off
int checkLight(){
  int holdLightVal = 0;
  for (int i = 0; i < 50; i++){
  holdLightVal += analogRead(lightPin); delay(10);}
  
  holdLightVal = holdLightVal/50;

  Serial.println(holdLightVal);
  
  return holdLightVal;
}


//------------------------------------------------------------------Main()------------------------------------------------------------------------------------------------------------------------------------------
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  //Setup sleep wake up condition
  esp_sleep_enable_timer_wakeup((conversionFactor * minuteConversion * sleepTime));

  issueState = "0"; 
}

void loop() {
  // put your main code here, to run repeatedly:;

  //Check the status of light and go forward
  //Light is On, we don't need to look at the microphone and will send the appropriate issue state
  if(checkLight() > lightThreshold)
  {
    //Does light on things
    Serial.println("LightOn");
    
    //set issue state variable
    issueState = "0";

    delay(5000);
    
    connectWiFi();
    
    //connect and send information to FM if possible
    if (wificlient.connect(Host.c_str(), Port))
    {
      sendIssueState(issueState);
    }
    else
    {
      for(int i = bufferLoop; i > 0; i--){
        if(storedIssueBuffer[i] == "5"){storedIssueBuffer[i] = issueState; break;}//once an empty spot has been found then break
      }
    }
  }
  else
  {
    //Does light off things
    Serial.println("Light Off");
    
    delay(5000);


    //takes initial readings to establish a baseline for the microphone
    for (int g = 0; g < numReadingsBase; g++){
      //comparison value
      ADC_VALUE = analogRead(ADCPin);

      //Serial.print(", ADC_VALUE: ");
      //Serial.println(ADC_VALUE);
      //if (ADC_VALUE > ADC_MAX){ADC_MAX = ADC_VALUE;}
      //if (ADC_VALUE < ADC_MIN){ADC_MIN = ADC_VALUE;}
      
      ADC_SUM += ADC_VALUE;

      delay(timeDelay);
      }
      
      ADC_AVG = ADC_SUM/numReadingsBase;
      noiselessAVG = ADC_AVG;

      ADC_VALUE = 0;
      ADC_SUM = 0;
      ADC_AVG = 0;


    //take all data above the established 'baseline' to determine the level of noise
    for (int g = 0; g < numReadings; g++){

      ADC_VALUE = analogRead(ADCPin);

      if (ADC_VALUE > noiselessAVG){
        
        posVal++;
        diffBuffer = ADC_VALUE - noiselessAVG;

        //The purpose of this is to get the 'spread' of values read by the aDC to determine 
        //if the there is water running
        if(diffBuffer < 11){plus10++;}
        else if(diffBuffer < 21){plus20++;}
        else if(diffBuffer < 31){plus30++;}
        else if(diffBuffer < 41){plus40++;}
        else if(diffBuffer < 51){plus50++;}
        else if(diffBuffer < 61){plus60++;}
        else if(diffBuffer < 71){plus70++;}
        else if(diffBuffer < 81){plus80++;}
        else if(diffBuffer < 91){plus90++;}
        else if(diffBuffer < 101){plus100++;}
        else if(diffBuffer < 111){plus110++;}
        else if(diffBuffer < 121){plus120++;}
        else if(diffBuffer < 131){plus130++;}
        else if(diffBuffer < 141){plus140++;}
        else if(diffBuffer < 151){plus150++;}
        else if(diffBuffer < 161){plus160++;}
        else if(diffBuffer < 171){plus170++;}
        else if(diffBuffer < 181){plus180++;}
        else if(diffBuffer < 191){plus190++;}
        else if(diffBuffer < 201){plus200++;}
        else if(diffBuffer < 211){plus210++;}
        else if(diffBuffer < 221){plus220++;}
        else if(diffBuffer < 231){plus230++;}
        else if(diffBuffer < 241){plus240++;}
        else if(diffBuffer < 251){plus250++;}
        else if(diffBuffer < 261){plus260++;}
        else if(diffBuffer < 271){plus270++;}
        else if(diffBuffer < 281){plus280++;}
        else if(diffBuffer < 291){plus290++;}
        else if(diffBuffer < 301){plus300++;}
        else if(diffBuffer < 311){plus310++;}
        else if(diffBuffer < 321){plus320++;}
        else if(diffBuffer < 331){plus330++;}
        else if(diffBuffer < 341){plus340++;}
        else if(diffBuffer < 351){plus350++;}
        else if(diffBuffer < 361){plus360++;}
        else if(diffBuffer < 371){plus370++;}
        else if(diffBuffer < 381){plus380++;}
        else if(diffBuffer < 391){plus390++;}
        else if(diffBuffer < 401){plus400++;}
        else if(diffBuffer < 411){plus410++;}
        else if(diffBuffer < 421){plus420++;}
        else if(diffBuffer < 431){plus430++;}
        else if(diffBuffer < 441){plus440++;}
        else {plus450++;}

        Serial.print("ADC_VALUE: ");
        Serial.println(ADC_VALUE);
      
        //if (ADC_VALUE > ADC_MAX){ADC_MAX = ADC_VALUE;}
        //if (ADC_VALUE < ADC_MIN){ADC_MIN = ADC_VALUE;}

        ADC_SUM += ADC_VALUE;
      }
        delay(timeDelay);
    }
      ADC_AVG = ADC_SUM/posVal;
      
//      Serial.print("ADC AVG: ");
//      Serial.print(ADC_AVG);
//      
//      Serial.print("ADC MIN: ");
//      Serial.print(ADC_MIN);
//      
//      Serial.print("ADC MAX: ");
//      Serial.print(ADC_MAX);
//      Serial.print(", ADCComp MAX: ");
//      Serial.println(ADCComp_MAX); 
//
//      Serial.print("Plus10: ");
//      Serial.println(plus10);
//      Serial.print("Plus20: ");
//      Serial.println(plus20);
//      Serial.print("Plus30: ");
//      Serial.println(plus30);
//      Serial.print("Plus40: ");
//      Serial.println(plus40);
//      Serial.print("Plus50: ");
//      Serial.println(plus50);
//      Serial.print("Plus60: ");
//      Serial.println(plus60);
//      Serial.print("Plus70: ");
//      Serial.println(plus70);
//      Serial.print("Plus80: ");
//      Serial.println(plus80);
//      Serial.print("Plus90: ");
//      Serial.println(plus90);
//      Serial.print("Plus100: ");
//      Serial.println(plus100);
//      Serial.print("Plus110: ");
//      Serial.println(plus110);
//      Serial.print("Plus120: ");
//      Serial.println(plus120);
//      Serial.print("Plus130: ");
//      Serial.println(plus130);
//      Serial.print("Plus140: ");
//      Serial.println(plus140);
//      Serial.print("Plus150: ");
//      Serial.println(plus150);
//      Serial.print("Plus160: ");
//      Serial.println(plus160);
//      Serial.print("Plus170: ");
//      Serial.println(plus170);
//      Serial.print("Plus180: ");
//      Serial.println(plus180);
//      Serial.print("Plus190: ");
//      Serial.println(plus190);
//      Serial.print("Plus200: ");
//      Serial.println(plus200);
//      Serial.print("Plus210: ");
//      Serial.println(plus210);
//      Serial.print("Plus220: ");
//      Serial.println(plus220);
//      Serial.print("Plus230: ");
//      Serial.println(plus230);
//      Serial.print("Plus240: ");
//      Serial.println(plus240);
//      Serial.print("Plus250: ");
//      Serial.println(plus250);
//      Serial.print("Plus260: ");
//      Serial.println(plus260);
//      Serial.print("Plus270: ");
//      Serial.println(plus270);
//      Serial.print("Plus280: ");
//      Serial.println(plus280);
//      Serial.print("Plus290: ");
//      Serial.println(plus290);
//      Serial.print("Plus300: ");
//      Serial.println(plus300);
//      Serial.print("Plus310: ");
//      Serial.println(plus310);
//      Serial.print("Plus320: ");
//      Serial.println(plus320);
//      Serial.print("Plus330: ");
//      Serial.println(plus330);
//      Serial.print("Plus340: ");
//      Serial.println(plus340);
//      Serial.print("Plus350: ");
//      Serial.println(plus350);
//      Serial.print("Plus360: ");
//      Serial.println(plus360);
//      Serial.print("Plus370: ");
//      Serial.println(plus370);
//      Serial.print("Plus380: ");
//      Serial.println(plus380);
//      Serial.print("Plus390: ");
//      Serial.println(plus390);
//      Serial.print("Plus400: ");
//      Serial.println(plus400);
//      Serial.print("Plus410: ");
//      Serial.println(plus410);
//      Serial.print("Plus420: ");
//      Serial.println(plus420);
//      Serial.print("Plus430: ");
//      Serial.println(plus430);
//      Serial.print("Plus440: ");
//      Serial.println(plus440);
//      Serial.print("Plus450: ");
//      Serial.println(plus450);
      
      
      //ADCSpreadMax = ADC_MAX - ADC_AVG;
      //ADCSpreadMin = ADC_AVG - ADC_MIN;
      //ADCSpreadTotal = ADC_MAX - ADC_MIN;
//
//      Serial.print("ADCSpreadMax: ");
//      Serial.print(ADCSpreadMax);
//
//      Serial.print("ADCSpreadMin: ");
//      Serial.print(ADCSpreadMin);  
//      
//      Serial.print("ADCSpreadTotal: ");
//      Serial.print(ADCSpreadTotal);
    

    //Condtion to send true or false
    //if the average value is larger than a tested threshold
    if ((ADC_AVG - noiselessAVG) > 20){issueState = "1";}
    //start comparing percentages of occurances to determine noise level
    else if ((plus10*1000/posVal) < 530){issueState = "1";}
    else if ((plus50*1000/posVal) > 7){issueState = "1";}
    else if (((plus10*1000/posVal) < 550) && ((plus30*1000/posVal) >120)){issueState = "1";}
    else {issueState = "0";}

      
    connectWiFi();

    //connect and send information to FM if possible
    if (wificlient.connect(Host.c_str(), Port))
    {
      sendIssueState(issueState);
    }
    else
    {
      for(int i = bufferLoop; i > 0; i--){
        if(storedIssueBuffer[i] == "5"){storedIssueBuffer[i] = issueState; break;}//once an empty spot has been found then break
      }
    }
    
  }
  
  //sent and data if it was stored in the buffer before hand, assuming a connection was made
  if (WiFi.status() == WL_CONNECTED){
    for(int k = 0; k < bufferLoop; k++){
     if(storedIssueBuffer[k] != "5")
      {
        //set issue state variable
        issueState = storedIssueBuffer[k];
        //send information to FM if possible
        if (wificlient.connect(Host.c_str(), Port))
        {
          sendIssueState(issueState);
        }
        //Reset stored value to "5" signifiying that 
        storedIssueBuffer[k] = "5";
      }
    }
  }
  
    Serial.println("EnterDeepSleep");
    esp_deep_sleep_start();
    Serial.println("This will never be printed");
}

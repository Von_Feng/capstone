import jsonpickle
import numpy as np
import cv2
import io 
import pytesseract 
import os
import xlsxwriter

def number_of_file(x):
    return(int(x[0:-4]))
workbook = xlsxwriter.Workbook('tessedit_output.xlsx')
worksheet = workbook.add_worksheet()
row = 1

index=0
single_digit_dir="./single_digit_images/"
paths =os.listdir(single_digit_dir)
paths=sorted(paths,key=number_of_file)
for file in paths:
    index +=1
    image = cv2.imread(os.path.join(single_digit_dir,file))
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)
    height, width = image.shape[:2]
    wide = cv2.Canny(blurred, 10, 200)
    text = pytesseract.image_to_string(gray, config='--psm 10 --oem 3 -c tessedit_char_whitelist=0123456789')
    print("-------------")
    #print(text)
    worksheet.write(row, 0,     os.path.join(single_digit_dir,file))
    worksheet.write(row, 1, text)
    row += 1
    #print(os.path.join(single_digit_dir,file))
    print("prcoess: ",index/len(paths))
workbook.close()
#=SUBSTITUTE(B3,"_x000C_","")

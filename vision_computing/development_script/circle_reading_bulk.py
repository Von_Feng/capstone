import numpy as np
import cv2
import math
import os
import tflite_runtime.interpreter as tflite
import xlsxwriter
import time
import json

circle_image_dir="./circle_images/"
model_path="../model_lite_version_3.h5"
tol=0.3

def image_convert(img):
    img=cv2.resize(img, (28,28), interpolation = cv2.INTER_AREA)
    img=img[..., np.newaxis]
    img = (np.expand_dims(img,0))
    return img.astype(np.float32)
    

def circle_image_to_reading_integer_output(image,image_rotation,setting=None):
    if image is not None:
        if image is not None:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            height, width = gray.shape
            json_string=setting
            #default setting
            if setting is None:
                json_string="[[0.625,0.213,0.767,0.284],[0.625,0.307,0.767,0.378],[0.625,0.401,0.767,0.472],[0.625,0.495,0.767,0.566],[0.625,0.590,0.767,0.660],[0.625,0.684,0.767,0.755]]" 
            counter_area=json.loads(json_string)
            interpreter = tflite.Interpreter(model_path)
            interpreter.allocate_tensors()
            # Get input and output tensors.
            input_details = interpreter.get_input_details()
            output_details = interpreter.get_output_details()
            class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
            class_names=np.concatenate((class_names,class_names,class_names), axis=0)
            whole_number=""

            for i in counter_area:

                #defined scope
                image_0=gray[int(height*i[0]):int(height*i[2]), int(width*i[1]):int(width*i[3])]
                image_0=image_convert(image_0)
                interpreter.set_tensor(input_details[0]['index'], image_0)
                interpreter.invoke()
                digit_0 =interpreter.get_tensor(output_details[0]['index'])

                #move left scope and increase height
                image_1=gray[int(height*(i[0]-(i[2]-i[0])*tol/2)):int(height*(i[2]+(i[2]-i[0])*tol/2)), int(width*(i[1]-(i[3]-i[1])*tol)):int(width*(i[3]-(i[3]-i[1])*tol))]
                image_1=image_convert(image_1)
                interpreter.set_tensor(input_details[0]['index'], image_1)
                interpreter.invoke()
                digit_1 =interpreter.get_tensor(output_details[0]['index'])

                #move right scope and increase height
                image_2=gray[int(height*(i[0]-(i[2]-i[0])*tol/2)):int(height*(i[2]+(i[2]-i[0])*tol/2)), int(width*(i[1]+(i[3]-i[1])*tol)):int(width*(i[3]+(i[3]-i[1])*tol))]
                image_2=image_convert(image_2)
                interpreter.set_tensor(input_details[0]['index'], image_2)
                interpreter.invoke()
                digit_2 =interpreter.get_tensor(output_details[0]['index'])

                
                digit=np.concatenate((digit_0,digit_1,digit_2),axis=0)
                whole_number += class_names[np.argmax(digit[0], axis=0)]

            return whole_number
        
        else:
            return None
    
    else:
        return None

workbook = xlsxwriter.Workbook('tf_output.xlsx')
worksheet = workbook.add_worksheet()
row = 1
  
paths =os.listdir(circle_image_dir)
paths=sorted(paths)
number_of_success=0
index=0
start = time.time()
for file in paths:
    index +=1
    image = cv2.imread(os.path.join(circle_image_dir,file))
    whole_number=circle_image_to_reading_integer_output(image,180)
    worksheet.write(row, 0, os.path.join(circle_image_dir,file))
    worksheet.write(row, 1, whole_number)
    print("-------------")
    print(os.path.join(circle_image_dir,file))
    print(whole_number)
    print("prcoess: ",index/len(paths))
    row += 1

end = time.time()
workbook.close()

print("the total time:  ",time.time() - start)
print("the number of images: ",len(paths))
print("average time:  ",(time.time() - start)/len(paths))
    
    


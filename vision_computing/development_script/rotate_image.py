import numpy as np
import cv2
import math
import os
import tflite_runtime.interpreter as tflite
import xlsxwriter
import time
import json

raw_image_dir="./circle_images"
circle_image_dir="./dataset/train_rotation/270"
image_rotation_deg=270

def raw_image_to_circle_image_output(image,image_rotation):
    if image_rotation==90:
        image=cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
    elif image_rotation==180:
        image=cv2.rotate(image, cv2.ROTATE_180)
    elif image_rotation==270:
        image=cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)  
    return image



paths =os.listdir(raw_image_dir)
paths=sorted(paths)
index=0
start = time.time()
for file in paths:
    index +=1
    image = cv2.imread(os.path.join(raw_image_dir,file))
    image_cut=raw_image_to_circle_image_output(image,image_rotation_deg)
    print(os.path.join(raw_image_dir,file))
    cv2.imwrite(os.path.join(circle_image_dir,"circle_"+file+'.jpg'), image_cut)
    print("-------------")
    print(os.path.join(raw_image_dir,file))
    print("prcoess: ",index/len(paths))


print("the total time:  ",time.time() - start)
print("the number of images: ",len(paths))
print("average time:  ",(time.time() - start)/len(paths))
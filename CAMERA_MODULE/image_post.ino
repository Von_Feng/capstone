#include <Arduino.h>
#include <WiFi.h>

//please download the Ethernet library
#include <Ethernet.h>

// in SPI.cpp, please change the last line of code to "SPIClass SPI(HSPI);"
// as we would need to use HSPI instead LSPI
#include <SPI.h>
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "esp_camera.h"
#include "esp_eth.h"
        

const char* ssid = "";
const char* password = "";
String Host = "www.feng.family";   
String Path = "/api/device_input";
const int Port = 80;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
WiFiClient wificlient;
EthernetClient ethernetclient;


// CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); 
  Serial.begin(115200);

  Ethernet.init(15);
  byte ethernet_mac[6];
  String wifi_mac=WiFi.macAddress();
  Serial.println(wifi_mac);
  char char_array[18];
  strcpy(char_array, wifi_mac.c_str()); 
  sscanf(char_array, "%02X:%02X:%02X:%02X:%02X:%02X", &ethernet_mac[0], &ethernet_mac[1], &ethernet_mac[2], &ethernet_mac[3], &ethernet_mac[4], &ethernet_mac[5]);
  ethernet_mac[3]=ethernet_mac[3]+1;
  ethernet_mac[4]=ethernet_mac[4]+5;
  ethernet_mac[5]=ethernet_mac[5]+2;

  
  Serial.println("Initialize Ethernet with DHCP:");
   if (Ethernet.begin(ethernet_mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    
  } else {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
  }
  
  WiFi.mode(WIFI_STA);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);  
  //attemp to connect the wifi at least 20 times
  for(int i=0;i<20;i++) {
    WiFi.begin(ssid, password);  
    Serial.print(".");
    if(WiFi.status() == WL_CONNECTED)
       break;
    delay(1000);
  }

  Serial.println();
  Serial.print("ESP32-CAM IP Address: ");
  Serial.println(WiFi.localIP());

  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;

  // init with high specs to pre-allocate larger buffers
  if(psramFound()){
    //config.frame_size = (framesize_t)10;
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 10;  //0-63 lower number means higher quality
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_SXGA;
    config.jpeg_quality = 12;  //0-63 lower number means higher quality
    config.fb_count = 1;
  }
  
  // camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    delay(1000);
    ESP.restart();
  }

}

void loop() {


  
  //image information
  camera_fb_t * fb = NULL;
  fb = esp_camera_fb_get();
  if(!fb) {
    Serial.println("Camera capture failed");
    delay(1000);
    ESP.restart();
  }
  
  //form information
  String head_0 = "--esp32boundary\r\nContent-Disposition: form-data; name=\"imageFile\"; filename=\"esp32-temimage.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n";
  String tail_0 = "\r\n--esp32boundary\r\n";
  
  String head_1 = "--esp32boundary\r\nContent-Disposition: form-data; name=\"mac_address\"; \r\nContent-Type: text/plain\r\n\r\n";
  String content_1=WiFi.macAddress();
  String tail_1 = "\r\n--esp32boundary\r\n";
  
  String head_2 = "--esp32boundary\r\nContent-Disposition: form-data; name=\"device_type\"; \r\nContent-Type: text/plain\r\n\r\n";
  String content_2="0";  
  String tail_2 = "\r\n--esp32boundary--\r\n";

  //message length
  uint32_t content_0_length = fb->len;
  uint32_t totalLen = head_0.length()+content_0_length+ tail_0.length()+head_1.length()+content_1.length() + tail_1.length()+head_2.length()+content_2.length() + tail_2.length();
  
  //ethernet has higher priority 
   if (ethernetclient.connect(Host.c_str(), Port))
  {
    Serial.println("ethernet Connection successful!");
    ethernetclient.println("POST " + Path + " HTTP/1.1");
    ethernetclient.println("Host: " + Host);
    ethernetclient.println("Content-Length: " + String(totalLen));
    ethernetclient.println("Content-Type: multipart/form-data; boundary=esp32boundary");
    ethernetclient.println();
    ethernetclient.print(head_0);

    //send content of image file
    uint8_t *fbBuf = fb->buf;
    size_t fbLen = fb->len;
    for (size_t n=0; n<fbLen; n=n+1024) {
      if (n+1024 < fbLen) {
        ethernetclient.write(fbBuf, 1024);
        fbBuf += 1024;
      }
      else if (fbLen%1024>0) {
        size_t remainder = fbLen%1024;
        ethernetclient.write(fbBuf, remainder);
      }
    }   
    ethernetclient.print(tail_0);

    //send mac address
    ethernetclient.print(head_1);
    ethernetclient.print(content_1);
    ethernetclient.print(tail_1);

    //send device type
    ethernetclient.print(head_2);
    ethernetclient.print(content_2);
    ethernetclient.print(tail_2);

    String getAll;
    String getBody;
    int timoutTimer = 10000;
    long startTimer = millis();
    boolean state = false;  
    while ((startTimer + timoutTimer) > millis()) {
      Serial.print(".");
      delay(100);      
      while (ethernetclient.available()) {
        char c = ethernetclient.read();
        if (c == '\n') {
          if (getAll.length()==0) { state=true; }
          getAll = "";
        }
        else if (c != '\r') { getAll += String(c); }
        if (state==true) { getBody += String(c); }
        startTimer = millis();
      }
      if (getBody.length()>0) { break; }
    }
    Serial.println();
    ethernetclient.stop();
    Serial.println(getBody);

    
  }
  else if (wificlient.connect(Host.c_str(), Port))
  {
    Serial.println("wifi Connection successful!");
    wificlient.println("POST " + Path + " HTTP/1.1");
    wificlient.println("Host: " + Host);
    wificlient.println("Content-Length: " + String(totalLen));
    wificlient.println("Content-Type: multipart/form-data; boundary=esp32boundary");
    wificlient.println();
    wificlient.print(head_0);
    
    //send content of image file
    uint8_t *fbBuf = fb->buf;
    size_t fbLen = fb->len;
    for (size_t n=0; n<fbLen; n=n+1024) {
      if (n+1024 < fbLen) {
        wificlient.write(fbBuf, 1024);
        fbBuf += 1024;
      }
      else if (fbLen%1024>0) {
        size_t remainder = fbLen%1024;
        wificlient.write(fbBuf, remainder);
      }
    }   
    wificlient.print(tail_0);

    //send mac address
    wificlient.print(head_1);
    wificlient.print(content_1);
    wificlient.print(tail_1);

    
    //send device type
    wificlient.print(head_2);
    wificlient.print(content_2);
    wificlient.print(tail_2);


    String getAll;
    String getBody;
    int timoutTimer = 10000;
    long startTimer = millis();
    boolean state = false;  
    while ((startTimer + timoutTimer) > millis()) {
      Serial.print(".");
      delay(100);      
      while (wificlient.available()) {
        char c = wificlient.read();
        if (c == '\n') {
          if (getAll.length()==0) { state=true; }
          getAll = "";
        }
        else if (c != '\r') { getAll += String(c); }
        if (state==true) { getBody += String(c); }
        startTimer = millis();
      }
      if (getBody.length()>0) { break; }
    }
    Serial.println();
    wificlient.stop();
    Serial.println(getBody);
     
  }
  else
  {
    Serial.println("no connection available");
  }
  
    // release the image buffer 
    esp_camera_fb_return(fb);
    delay(10000);
}

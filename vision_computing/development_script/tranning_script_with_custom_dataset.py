import numpy as np
import os
import PIL
import PIL.Image
import tensorflow as tf
import pathlib
#import tensorflow_datasets as tfds
from tensorflow.keras import layers




single_digit_dir="./single_digit_images/"  
data_dir="./dataset/train"
batch_size = 32
img_height = 28
img_width = 28
class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

def decode_img(img):
  img= tf.io.read_file(img)
  img = tf.image.decode_png(img, channels=1) 
  img = tf.image.resize(img, [img_height, img_width])
  img = (np.expand_dims(img,0))
  return img


train_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="training",
  seed=123,
  color_mode='grayscale',
  image_size=(img_height, img_width),
  batch_size=batch_size)

val_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="validation",
  seed=132,
  color_mode='grayscale',
  image_size=(img_height, img_width),
  batch_size=batch_size)


AUTOTUNE = tf.data.experimental.AUTOTUNE

train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
num_classes = 10

model = tf.keras.Sequential([
  layers.experimental.preprocessing.Rescaling(1./255),
  layers.Conv2D(32, (3, 3), activation='relu',input_shape=(28, 28, 1)),
  layers.Conv2D(64, (3, 3), activation='relu'),
  layers.MaxPooling2D((2, 2)),
  layers.Dropout(0.25),
  layers.Flatten(),
  layers.Dense(256, activation='relu'),
  layers.Dropout(0.5),
  layers.Dense(num_classes, activation='softmax')
])

model.compile(
  optimizer='adam',
  loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
  metrics=['accuracy'])

model.fit(
  train_ds,
  validation_data=val_ds,
  epochs=80
)


model.save('model_custom_dataset.h5')
converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()

# Save the model.
with open('model_lite_custom_dataset.h5', 'wb') as f:
  f.write(tflite_model)

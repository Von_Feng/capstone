import numpy as np
import cv2
import math
import os
import tflite_runtime.interpreter as tflite
import xlsxwriter
import time
import json

raw_image_dir="../raw_images/"
circle_image_dir="./circle_images/"
#model_path="../final_model_lite_own.h5"

def image_convert(img):
    img=cv2.resize(img, (28,28), interpolation = cv2.INTER_AREA)
    img = (np.expand_dims(img,0))
    return img.astype(np.float32)
    

def raw_image_to_circle_image_output(image,image_rotation):
    image=np.array(image)
    x,y,channel=image.shape
    #target piexl=400x296 and keep ratio of width and height of images
    x_ratio=x/768
    y_ratio=y/1024
    final_ratio=0
    if(x_ratio>y_ratio):
        final_ratio=y_ratio
    else:
        final_ratio=x_ratio   
    x=int(x/final_ratio)
    y=int(y/final_ratio)  

    image_resized=cv2.resize(image, (y,x), interpolation = cv2.INTER_AREA)
    image=image_resized
    gray = cv2.cvtColor(image_resized, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), cv2.BORDER_DEFAULT)
    height, width = image_resized.shape[:2]
    distance_to_center=0
    minDist=0
    if height >width:
        minDist=width/2
        distance_to_center=height
    else:
        minDist=height/2
        distance_to_center=width

    # apply Canny edge detection using a wide threshold
    # threshold, and automatically determined threshold
    wide = cv2.Canny(blurred, 10, 200)
    circles = cv2.HoughCircles(wide , cv2.HOUGH_GRADIENT, 1, minDist) 
    # ensure at least some circles were found
    max_r=0
    max_x=0
    max_y=0

    if circles is not None:
        # convert the (x, y) coordinates and radius of the circles to integers
        circles = np.round(circles[0, :]).astype("int")
        # loop over the (x, y) coordinates and radius of the circles
        # find the the least distance to center of the image
        
        for (x, y, r) in circles:
            distance_to_center_tem=math.sqrt((x-width/2)*(x-width/2)+(y-height/2)*(y-height/2))
            if distance_to_center_tem<distance_to_center:
                max_r=r
                max_x=x
                max_y=y
                distance_to_center=distance_to_center_tem
                
        # find the small radius with 10 & torrloance distance 
        for (x, y, r) in circles:
            distance_to_center_tem=math.sqrt((x-width/2)*(x-width/2)+(y-height/2)*(y-height/2))
            # 10% torrrolence
            if distance_to_center_tem<distance_to_center*1.1 and r<max_r:
                max_r=r
                max_x=x
                max_y=y
                distance_to_center=distance_to_center_tem

        #max_r=int(max_r*final_ratio)
        #max_x=int(max_x*final_ratio)  
        #max_y=int(max_y*final_ratio)
        # show the output image
        #cv2.circle(image, (max_x, max_y), max_r, (0, 255, 0), 4)
        image_cut=image[max_y-max_r:max_y+max_r,max_x-max_r:max_x+max_r]
        if image_rotation==90:
            image_cut=cv2.rotate(image_cut, cv2.ROTATE_90_CLOCKWISE)
        elif image_rotation==180:
            image_cut=cv2.rotate(image_cut, cv2.ROTATE_180)
        elif image_rotation==270:
            image_cut=cv2.rotate(image_cut, cv2.ROTATE_90_COUNTERCLOCKWISE)  
        return image_cut
 

    else:
        return image


  
paths =os.listdir(raw_image_dir)
paths=sorted(paths)
index=0
start = time.time()
for file in paths:
    index +=1
    image = cv2.imread(os.path.join(raw_image_dir,file))
    image_cut=raw_image_to_circle_image_output(image,180)
    print(os.path.join(raw_image_dir,file))
    cv2.imwrite(os.path.join(circle_image_dir,"circle_"+file+'.jpg'), image_cut)
    print("-------------")
    print(os.path.join(raw_image_dir,file))
    print("prcoess: ",index/len(paths))


print("the total time:  ",time.time() - start)
print("the number of images: ",len(paths))
print("average time:  ",(time.time() - start)/len(paths))

    
    


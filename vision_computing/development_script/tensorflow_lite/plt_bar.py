import tflite_runtime.interpreter as tflite
import matplotlib.pyplot as plt
import numpy as np
import os
import cv2


def image_open_and_convert(image_path):
    image=cv2.imread(image_path)
    image=cv2.resize(image, (28,28), interpolation = cv2.INTER_AREA)
    image = (np.expand_dims(image,0))
    return image.astype(np.float32)

def plot_value_array_owned(image,predictions_array,true_label):
  plt.subplot(1,2,1)
  plt.imshow(image)
  plt.subplot(1,2,2)
  plt.bar(range(10), predictions_array, tick_label = true_label, width = 0.8, color = ['red', 'green']) 
  plt.xlabel('x - axis')
  plt.ylabel('y - axis') 
  plt.title('weight') 
  plt.show()


single_digit_dir="../single_digit_images/"
interpreter = tflite.Interpreter('../final_model_lite_own.h5')
interpreter.allocate_tensors()


  
# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()


#image = cv2.imread(os.path.join(single_digit_dir,'1.png'))
# Test the model on random input data.
#input_shape = input_details[0]['shape']
#gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#blurred = cv2.GaussianBlur(gray, (3, 3), 0)
img=image_open_and_convert(os.path.join(single_digit_dir,'2.png'))
image = cv2.imread(os.path.join(single_digit_dir,'2.png'))
#input_data = np.array(np.random.random_sample(input_shape), dtype=np.float32)
interpreter.set_tensor(input_details[0]['index'], img)

interpreter.invoke()

# The function `get_tensor()` returns a copy of the tensor data.
# Use `tensor()` in order to get a pointer to the tensor.
output_data = interpreter.get_tensor(output_details[0]['index'])

class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
print(output_data[0])
plot_value_array_owned(image,output_data[0],class_names)

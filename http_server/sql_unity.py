import sqlite3
import json
from flask import Flask, request, Response,render_template
from datetime import datetime, timedelta
import csv
import io
database="../../http_server_env/water.db"
default_setting="[[0.625,0.213,0.767,0.284],[0.625,0.307,0.767,0.378],[0.625,0.401,0.767,0.472],[0.625,0.495,0.767,0.566],[0.625,0.590,0.767,0.660],[0.625,0.684,0.767,0.755]]" 



def value_list(location=None,start_time=None,end_time=None,number_of_values=None):
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()
    if number_of_values is None:
        number_of_values=100
    if location is not None and start_time is not None and end_time is not None:
        mycursor.execute("SELECT value_list.location, value_list.value,value_list.time,'value_list'.'index' FROM value_list WHERE value_list.location='"+str(location)+"' AND value_list.time BETWEEN '"+str(start_time)+"' AND '"+str(end_time)+"' ORDER BY value_list.time DESC LIMIT "+str(number_of_values)+";")

    elif end_time is not None and location is not None:
        mycursor.execute("SELECT value_list.location, value_list.value,value_list.time,'value_list'.'index' FROM value_list WHERE value_list.location='"+str(location)+"' AND value_list.time < '"+str(end_time)+"' ORDER BY value_list.time DESC LIMIT "+str(number_of_values)+";")

    elif start_time is not None and location is not None:
        mycursor.execute("SELECT value_list.location, value_list.value,value_list.time,'value_list'.'index' FROM value_list WHERE value_list.location='"+str(location)+"' AND value_list.time > '"+str(start_time)+"' ORDER BY value_list.time DESC LIMIT "+str(number_of_values)+";")

    elif start_time is not None and end_time is not None:
        mycursor.execute("SELECT value_list.location, value_list.value,value_list.time,'value_list'.'index' FROM value_list WHERE value_list.time BETWEEN '"+str(start_time)+"' AND '"+str(end_time)+"' ORDER BY value_list.time DESC LIMIT "+str(number_of_values)+";")

    elif location is not None:
        mycursor.execute("SELECT value_list.location, value_list.value,value_list.time,'value_list'.'index' FROM value_list WHERE value_list.location='"+str(location)+"' ORDER BY value_list.time DESC LIMIT "+str(number_of_values)+";")

    elif start_time is not None:
        mycursor.execute("SELECT value_list.location, value_list.value,value_list.time,'value_list'.'index' FROM value_list WHERE value_list.time > '"+str(start_time)+"' ORDER BY value_list.time DESC LIMIT "+str(number_of_values)+";")

    elif end_time is not None:
        mycursor.execute("SELECT value_list.location, value_list.value,value_list.time,'value_list'.'index' FROM value_list WHERE value_list.time < '"+str(end_time)+"' c "+str(number_of_values)+";")
    else:
        mycursor.execute("SELECT value_list.location, value_list.value,value_list.time ,'value_list'.'index' FROM value_list ORDER BY value_list.time DESC LIMIT "+str(number_of_values)+";")
    #mycursor.execute(sql_statement)
    json_obj = json.dumps(mycursor.fetchall(), indent=4, sort_keys=True, default=str)
    mycursor.close()
    mydb.close()
    return Response(response= json_obj, status=200, mimetype="application/json")


def array_object_sorted_by_location():
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()
    mycursor.execute("SELECT DISTINCT value_list.location FROM value_list")
    result_0 = mycursor.fetchall()   
    json_obj=[]
    for x in result_0:
        a_record={}
        cursor_local = mydb.cursor()
        cursor_local.execute("SELECT value_list.value, value_list.time, value_list.user_entry FROM value_list WHERE value_list.location='"+str(x[0])+"' ORDER BY value_list.time DESC")
        a_record['location']=x[0]
        a_record['values'] = cursor_local.fetchall()
        json_obj.append(a_record)
        cursor_local.close()
    mycursor.close()
    mydb.close()
    return json_obj

def optimized_array_object_sorted_by_location(max_periodical_change=0):
    json_obj=array_object_sorted_by_location()
    if (max_periodical_change is None or max_periodical_change==0):
        return json_obj
    else:
        for x in json_obj:
            a_location=[]
            previous_value=0
            previous_time=0
            current_value=0
            current_time=0
            x['values'].reverse()
            for y in x['values']:
                if(previous_time==0 or y[2]==1):
                    previous_value=y[0]
                    previous_time=datetime.strptime(y[1],'%Y-%m-%d %H:%M:%S')
                    current_value=previous_value
                    current_time=previous_time
                    a_location.append(y)
                else:
                    current_value=y[0]
                    current_time=datetime.strptime(y[1],'%Y-%m-%d %H:%M:%S')
                    value_dffference=current_value-previous_value
                    time_difference=current_time-previous_time

                    ratio=value_dffference/time_difference.total_seconds()
                    if (ratio<=max_periodical_change and current_value>=previous_value):
                        a_location.append(y)
                        previous_value=current_value
                        previous_time=current_time
            a_location.reverse()
            x['values']=a_location 
        return json_obj




def csv_file(ratio=0):
    json_obj=optimized_array_object_sorted_by_location(ratio)
    organzied_obj=[]
    #organzie the array
    #  
    for x in json_obj:
        for y in x['values']:
            a_record=[]
            a_record.append(x['location'])
            a_record.append(y[0])
            a_record.append(y[1])
            organzied_obj.append(a_record)

    output = io.StringIO()
    writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)
    writer.writerows(organzied_obj)
    return Response(response= output.getvalue(), status=200, mimetype="text/csv", headers={"Content-disposition":"attachment; filename="+str(datetime.now())+".csv"})

    
def value_list_graph():  
    json_obj = json.dumps(array_object_sorted_by_location(), indent=4, sort_keys=True, default=str)
    return Response(response= json_obj, status=200, mimetype="application/json")

def value_list_graph2(ratio=0):  
    json_obj = json.dumps(optimized_array_object_sorted_by_location(ratio), indent=4, sort_keys=True, default=str)
    return Response(response= json_obj, status=200, mimetype="application/json")

def summary_list_graph():
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()
    mycursor.execute("SELECT DISTINCT value_list.location FROM value_list WHERE value_list.value_type=1 AND value_list.time > '"+str(datetime.now()-timedelta(seconds=10))[0:19]+"';")
    result=mycursor.fetchall()
    mycursor.close()
    mydb.close()
    return Response(response=result , status=200, mimetype="application/json")


def location_list():
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()
    mycursor.execute("SELECT DISTINCT value_list.location FROM value_list")
    json_obj = json.dumps(mycursor.fetchall(), indent=4, sort_keys=True, default=str)
    mycursor.close()
    mydb.close()
    return Response(response= json_obj, status=200, mimetype="application/json")




def new_device_list():
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()
    mycursor.execute("SELECT id, location, device_type, check_point FROM device_list WHERE location='no' ORDER BY check_point DESC")
    json_obj = json.dumps(mycursor.fetchall(), indent=4, sort_keys=True, default=str)
    mycursor.close()
    mydb.close()
    return Response(response= json_obj, status=200, mimetype="application/json")


def registered_device_list():
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()
    mycursor.execute("SELECT id, location, device_type, check_point FROM device_list WHERE location!='no' ORDER BY check_point DESC")
    json_obj = json.dumps(mycursor.fetchall(), indent=4, sort_keys=True, default=str)
    mycursor.close()
    mydb.close()
    return Response(response= json_obj, status=200, mimetype="application/json")



def new_device_profile(mac_address,device_type):
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()   
    mycursor.execute("INSERT INTO device_list (mac_address,device_type) VALUES ('"+str(mac_address)+"',"+str(device_type)+")")
    mydb.commit()
    mycursor.close()
    mydb.close()

def device_id(mac_address):
    try:
        mydb = sqlite3.connect(database)
        mycursor = mydb.cursor()
        mycursor.execute("SELECT id FROM device_list WHERE mac_address='"+str(mac_address)+"'")     
        result = mycursor.fetchall()
        mycursor.close()
        mydb.close()
        if len(result) == 0:
            return 0
        else:
            return result[0][0]
    except Exception:
        return 0
        
def device_rotation(id):
    try:
        mydb = sqlite3.connect(database)
        mycursor = mydb.cursor()
        mycursor.execute("SELECT rotation FROM device_list WHERE id="+str(id))     
        result = mycursor.fetchall()
        mycursor.close()
        mydb.close()
        if len(result) == 0:
            return 0
        else:
            return result[0][0]
            
    except Exception:
        return 0

def device_flashlight_by_macaddress(mac_address):
    return device_flashlight(device_id(mac_address))



def device_flashlight(id):
    try:
        mydb = sqlite3.connect(database)
        mycursor = mydb.cursor()
        mycursor.execute("SELECT flashlight FROM device_list WHERE id="+str(id))     
        result = mycursor.fetchall()
        mycursor.close()
        mydb.close()
        if len(result) == 0:
            return 1
        else:
            return result[0][0]
            
    except Exception:
        return 1

def device_duty_cycle_by_macaddress(mac_address):
    return device_duty_cycle(device_id(mac_address))


def device_duty_cycle(id):
    try:
        mydb = sqlite3.connect(database)
        mycursor = mydb.cursor()
        mycursor.execute("SELECT duty_cycle FROM device_list WHERE id="+str(id))     
        result = mycursor.fetchall()
        mycursor.close()
        mydb.close()
        if len(result) == 0:
            return 125
        else:
            return result[0][0]
            
    except Exception:
        return 125

def device_location(id):
    try:
        mydb = sqlite3.connect(database)
        mycursor = mydb.cursor()
        mycursor.execute("SELECT location FROM device_list WHERE id="+str(id))     
        result = mycursor.fetchall()
        mycursor.close()
        mydb.close()
        if len(result) == 0:
            return 'no'
        else:
            return result[0][0]
            
    except Exception:
        return 'no'

def device_location_rotation_setting(id):
    try:
        mydb = sqlite3.connect(database)
        mycursor = mydb.cursor()
        mycursor.execute("SELECT location,rotation,setting FROM device_list WHERE id="+str(id))     
        result = mycursor.fetchall()
        mycursor.close()
        mydb.close()
        print(result)
        if len(result) == 0:            
            return 'no',0,default_setting
        else:
            return result[0][0],result[0][1],result[0][2]
            
    except Exception:
        return 'no',0,default_setting

def device_setting(id):
    try:
        
        mydb = sqlite3.connect(database)
        mycursor = mydb.cursor()
        mycursor.execute("SELECT setting FROM device_list WHERE id="+str(id))
        result = mycursor.fetchall()
        mycursor.close()
        mydb.close()
        if len(result) == 0:
            return default_setting
        else:
            if result[0][0] is None:
                return default_setting
            else:
                return result[0][0]
            
    except Exception:
        return  default_setting


def device_profile_update(id,location,setting=None,rotation=None,flashlight=1,duty_cycle=125):
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()
    if rotation is None:
        rotation=0
    if setting is not None:
         mycursor.execute("UPDATE device_list SET check_point=datetime('now','localtime') , location='"+str(location)+"', setting='"+str(setting)+"', rotation="+str(rotation)+",flashlight="+str(flashlight)+",duty_cycle="+str(duty_cycle)+" WHERE id="+str(id))
    else:
         mycursor.execute("UPDATE device_list SET check_point=datetime('now','localtime') , location='"+str(location)+"', setting=NULL, rotation="+str(rotation)+" WHERE id="+str(id))
    mydb.commit()
    mycursor.close()
    mydb.close()



def device_checkpoint_update(id):
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()
    mycursor.execute("UPDATE device_list SET check_point=datetime('now','localtime') WHERE id="+str(id))
    mydb.commit()
    mycursor.close()
    mydb.close()
        
def new_value(id,value,user_entry=0):
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()
    #insert new value
    mycursor.execute("INSERT INTO value_list (location,value_type,value,user_entry) SELECT device_list.location, device_list.device_type, "+str(value)+","+str(user_entry)+ " FROM device_list WHERE  device_list.id="+str(id)+";")
    mydb.commit()

    #retrieve the latest index of value with the device_id
    mycursor.execute("SELECT 'value_list'.'index', 'value_list'.'location' FROM value_list INNER JOIN device_list ON value_list.location=device_list.location WHERE  device_list.id="+str(id)+" ORDER BY value_list.time DESC LIMIT 1;")
    result = mycursor.fetchall()
    mycursor.close()
    mydb.close()
    return result[0][0],result[0][1]

def change_data_value(index,value):
    mydb = sqlite3.connect(database)
    mycursor = mydb.cursor()
    mycursor.execute("UPDATE value_list SET value="+str(value)+", user_entry='1' WHERE 'value_list'.'index'="+str(index))
    mydb.commit()
    mycursor.close()
    mydb.close()
        

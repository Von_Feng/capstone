sudo apt update
sudo apt install -y python3-venv
python3 -m venv ../../../http_server_env
source ../../../http_server_env/bin/activate
pip3 install wheel
pip3 install uwsgi flask
deactivate
python3 sqlite_set_up.py
sudo cp pyvenv_2.cfg ../../../http_server_env/pyvenv.cfg
sudo cp http_server.service /etc/systemd/system/http_server.service
sudo systemctl start  http_server
sudo systemctl enable  http_server
sudo systemctl status  http_server
sudo cp http_server /etc/nginx/sites-available/http_server
sudo ln -s /etc/nginx/sites-available/http_server /etc/nginx/sites-enabled
sudo rm /etc/nginx/sites-enabled/default
sudo nginx -t
sudo systemctl restart nginx
sudo ufw allow 'Nginx Full'

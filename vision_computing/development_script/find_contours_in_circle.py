import numpy as np
import pytesseract 
import argparse
import imutils
import cv2
import os
import json

circle_dir="./circle_images/"
single_digit_dir="./single_digit_images"

cnt_space=40
cnt_x=90
cnt_y=265
cnt_w=30
cnt_h=60
image_number=0
paths =os.listdir(circle_dir)
paths=sorted(paths,key=number_of_file)
number_of_success=0
index=0
tol=0.25
tol1=0.1
for file in paths:
    index +=1
    image = cv2.imread(os.path.join(circle_dir,file))
    #image = cv2.rotate(image, cv2.ROTATE_180)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)
    height, width = image.shape[:2]
    wide = cv2.Canny(blurred, 10, 200)
    cnts = cv2.findContours(wide, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    json_string="[[0.625,0.213,0.767,0.284],[0.625,0.307,0.767,0.378],[0.625,0.401,0.767,0.472],[0.625,0.495,0.767,0.566],[0.625,0.590,0.767,0.660],[0.625,0.684,0.767,0.755]]"  
    counter_area=json.loads(json_string)
    
    for i in counter_area:
        image_number+=1
        cv2.imwrite(os.path.join(single_digit_dir,str(image_number)+"a.png"),gray[int(height*i[0]):int(height*i[2]), int(width*i[1]):int(width*i[3])])
        # + tolerrrance 
        image_number+=1
        cv2.imwrite(os.path.join(single_digit_dir,str(image_number)+"a.png"),gray[int(height*(i[0]-(i[2]-i[0])*tol) ):int(height*(i[2]+(i[2]-i[0])*tol)), int(width*(i[1]-(i[3]-i[1])*tol)):int(width*(i[3]+(i[3]-i[1])*tol))])
        # - tolerrrance 
        image_number+=1
        cv2.imwrite(os.path.join(single_digit_dir,str(image_number)+"a.png"),gray[int(height*(i[0]+(i[2]-i[0])*tol1) ):int(height*(i[2]-(i[2]-i[0])*tol1)), int(width*(i[1]+(i[3]-i[1])*tol1)):int(width*(i[3]-(i[3]-i[1])*tol1))])
    print("-------------")
    print(os.path.join(circle_dir,file))
    print("prcoess: ",index/len(paths))


from picamera import PiCamera
import time
from uuid import getnode as get_mac
import io
import requests

start = time.time()
path="http://admin.feng.family/api/device_input"
mac_address=get_mac()
device_type=0
pic_IObytes = io.BytesIO()
camera = PiCamera()
camera.capture(pic_IObytes,format='png')
pic_IObytes.seek(0)
post_form={'device_type':device_type,'mac_address':mac_address}
x = requests.post(path, data = post_form,files={'imageFile':pic_IObytes.read()})
print(x.text)
end = time.time()
print(end - start)

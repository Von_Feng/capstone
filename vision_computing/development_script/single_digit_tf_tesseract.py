import tensorflow as tf
import numpy as np
import numpy as np
import cv2
import io 
import pytesseract 
import os
import xlsxwriter

def decode_img(img):
  img= tf.io.read_file(img)
  img = tf.image.decode_png(img, channels=3) 
  img = tf.image.resize(img, [28, 28])
  img = (np.expand_dims(img,0))
  return img


def number_of_file(x):
    return(int(x[0:-4]))
workbook = xlsxwriter.Workbook('tessedit_output.xlsx')
worksheet = workbook.add_worksheet()
#worksheet.set_default_row(0.76/0.18*15)
row = 1

index=0
single_digit_dir="./single_digit_images/"
paths =os.listdir(single_digit_dir)
paths=sorted(paths,key=number_of_file)
model = tf.keras.models.load_model('final_model_own.h5')


for file in paths:
    index +=1
    image = cv2.imread(os.path.join(single_digit_dir,file))
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)
    height, width = image.shape[:2]
    wide = cv2.Canny(blurred, 10, 200)
    text = pytesseract.image_to_string(gray, config='--psm 10 --oem 3 -c tessedit_char_whitelist=0123456789')
    
    img = decode_img(os.path.join(single_digit_dir,file))
    digit = model.predict(img)
    print("-------------")
    #print(text)
    worksheet.write(row, 0, file)
    #worksheet.insert_image(row,1,os.path.join(single_digit_dir,file),{'object_position': 2})
    worksheet.write(row, 3, text)
    worksheet.write(row, 5, np.argmax(digit[0], axis=0))
    row += 1
    #print(os.path.join(single_digit_dir,file))
    print("prcoess: ",index/len(paths))

workbook.close()
#=SUBSTITUTE(B3,"_x000C_","")

import numpy as np
import cv2
import math
import os
import tflite_runtime.interpreter as tflite
import xlsxwriter
import time
import json

circle_image_dir="./circle_images/"
model_path="../final_model_lite_own.h5"
tol=0.3


file="circle_2020_12_11_15_15_50_esp32-cam_jpg.jpg"
image = cv2.imread(os.path.join(circle_image_dir,file))
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
height, width = gray.shape

json_string="[[0.625,0.213,0.767,0.284],[0.625,0.307,0.767,0.378],[0.625,0.401,0.767,0.472],[0.625,0.495,0.767,0.566],[0.625,0.590,0.767,0.660],[0.625,0.684,0.767,0.755]]" 
counter_area=json.loads(json_string)
for i in counter_area:

    #defined scope
    image=cv2.cvtColor(gray[int(height*i[0]):int(height*i[2]), int(width*i[1]):int(width*i[3])],cv2.COLOR_GRAY2RGB)
    cv2.imshow('circle image',image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    #move left scope and increase height
    image=cv2.cvtColor(gray[int(height*(i[0]-(i[2]-i[0])*tol/2)):int(height*(i[2]+(i[2]-i[0])*tol/2)), int(width*(i[1]-(i[3]-i[1])*tol)):int(width*(i[3]-(i[3]-i[1])*tol))],cv2.COLOR_GRAY2RGB)
    cv2.imshow('circle image',image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #move right scope and increase height
    image=cv2.cvtColor(gray[int(height*(i[0]-(i[2]-i[0])*tol/2)):int(height*(i[2]+(i[2]-i[0])*tol/2)), int(width*(i[1]+(i[3]-i[1])*tol)):int(width*(i[3]+(i[3]-i[1])*tol))],cv2.COLOR_GRAY2RGB)
    cv2.imshow('circle image',image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

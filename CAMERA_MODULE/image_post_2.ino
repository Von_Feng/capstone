#include <Arduino.h>
#include <WiFi.h>

//please download the Arduino_JSON library
#include <Arduino_JSON.h>

//please download the Ethernet library
#include <Ethernet.h>
// in SPI.cpp, please change the last line of code to "SPIClass SPI(HSPI);"
// as we would need to use HSPI instead LSPI
#include <SPI.h>
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "esp_camera.h"
#include "esp_eth.h"

// setting PWM
const int freq = 5000;
const int ledChannel = 4;
const int pwm_resolution = 8;
const int flashlightPin = 4;

const char* ssid = "";
const char* password = "";
String Host = "www.feng.family";
String Path_flashlight = "/api/device_flashlight_and_duty_circle";
String Path = "/api/device_input";
const int Port = 80;
byte ethernet_mac[] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
WiFiClient wificlient;
EthernetClient ethernetclient;

int default_flashligt = 1;
int duty_cycle = 125;

// CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
  Serial.begin(115200);
  
  byte ethernet_mac[6];
  String wifi_mac = WiFi.macAddress();
  Serial.println("wifi mac_address: ");
  Serial.println(wifi_mac);
  char char_array[18];
  strcpy(char_array, wifi_mac.c_str());
  sscanf(char_array, "%02X:%02X:%02X:%02X:%02X:%02X", &ethernet_mac[0], &ethernet_mac[1], &ethernet_mac[2], &ethernet_mac[3], &ethernet_mac[4], &ethernet_mac[5]);
  ethernet_mac[3] = ethernet_mac[3] + 1;
  ethernet_mac[4] = ethernet_mac[4] + 5;
  ethernet_mac[5] = ethernet_mac[5] + 2;
  Serial.println("ethernet mac_address: ");
  Serial.print(ethernet_mac[0], HEX);
  Serial.print("-");
  Serial.print(ethernet_mac[1], HEX);
  Serial.print("-");
  Serial.print(ethernet_mac[2], HEX);
  Serial.print("-");
  Serial.print(ethernet_mac[3], HEX);
  Serial.print("-");
  Serial.print(ethernet_mac[4], HEX);
  Serial.print("-");
  Serial.print(ethernet_mac[5], HEX);
  Serial.println("");
  Ethernet.init(15);
  //connect to network
    Serial.println("Initialize Ethernet with DHCP:");
   if (Ethernet.begin(ethernet_mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
      wifi_connection();

  } else {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
  }
  
  

  //led set up
  ledcSetup(ledChannel, freq, pwm_resolution);
  ledcAttachPin(flashlightPin, ledChannel);

  //camera configuratio
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;

  // init with high specs to pre-allocate larger buffers
  if (psramFound()) {
    //config.frame_size = (framesize_t)10;
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 10;  //0-63 lower number means higher quality
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_SXGA;
    config.jpeg_quality = 12;  //0-63 lower number means higher quality
    config.fb_count = 1;
  }

  // camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    delay(1000);
    ESP.restart();
  }


}

void loop() {

  if (Ethernet.linkStatus() == LinkON)
  {
    WiFi.disconnect();
    set_flashlight(get_flashlight_setting(ethernetclient));
  }
  else
  {
      if (WiFi.status() != WL_CONNECTED)
      {
       wifi_connection();
      }
      set_flashlight(get_flashlight_setting(wificlient));
  }

  wificlient.stop();
  ethernetclient.stop();
  
  if (default_flashligt == 1)
  { ledcWrite(ledChannel, duty_cycle);
    Serial.print("set duty_cycle: ");
    Serial.println(duty_cycle);
    Serial.println("flashlight on");
  }

  delay(100);
  //image information
  camera_fb_t * fb = NULL;
  fb = esp_camera_fb_get();
  if (!fb) {
    Serial.println("Camera capture failed");
    delay(1000);
    ESP.restart();
  }
  delay(100);
  ledcWrite(ledChannel, 0);


  if (Ethernet.linkStatus() == LinkON)
  {
    WiFi.disconnect();
    send_image_form(ethernetclient, fb);
  }
  else
  {
 
      if (WiFi.status() != WL_CONNECTED)
      {
        wifi_connection();
      }
      send_image_form(wificlient, fb);
  }

  wificlient.stop();
  ethernetclient.stop();
  
  // release the image buffer
  esp_camera_fb_return(fb);
  delay(10000);
}


bool wifi_connection()
{

  WiFi.mode(WIFI_STA);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  //attemp to connect the wifi at least 20 times
  for (int i = 0; i < 20; i++) {
    WiFi.begin(ssid, password);
    Serial.print(".");
    if (WiFi.status() == WL_CONNECTED)
      break;
    delay(1000);
  }

  Serial.println();
  Serial.print("WIFI IP Address: ");
  Serial.println(WiFi.localIP());
  if (WiFi.status() == WL_CONNECTED)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool ethernet_connection()
{
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(ethernet_mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }

  } else {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
    return true;
  }

  return false;
}

void set_flashlight(String a)
{
  JSONVar json_Object = JSON.parse(a);

  // JSON.typeof(json_Object ) can be used to get the type of the var
  if (JSON.typeof(json_Object) == "undefined") {
    Serial.println("Parsing input failed!");
    default_flashligt = true;
    duty_cycle = 125;

  }
  else
  {
    default_flashligt = (int)json_Object["flashlight"];
    duty_cycle = (int)json_Object["duty_cycle"];
  }
}



template<class T>
String get_flashlight_setting(T connection_client)
{

  String getAll = "";
  String getBody = "";
  int timoutTimer = 20000;
  long startTimer = millis();
  boolean state = false;


  //form information
  String head_0 = "--esp32boundary\r\nContent-Disposition: form-data; name=\"mac_address\"; \r\nContent-Type: text/plain\r\n\r\n";
  String content_0 = WiFi.macAddress();
  String tail_0 = "\r\n--esp32boundary--\r\n";


  //get flashlight setting
  uint32_t totalLen = head_0.length() + content_0.length() + tail_0.length();
  //ethernet has higher priority
  if (connection_client.connect(Host.c_str(), Port))
  {
    Serial.println("client level Connection successful!");
    connection_client.println("POST " + Path_flashlight + " HTTP/1.1");
    connection_client.println("Host: " + Host);
    connection_client.println("Content-Length: " + String(totalLen));
    connection_client.println("Content-Type: multipart/form-data; boundary=esp32boundary");
    connection_client.println();
    //send mac address
    connection_client.print(head_0);
    connection_client.print(content_0);
    connection_client.print(tail_0);

    while ((startTimer + timoutTimer) > millis()) {
      Serial.print(".");
      delay(100);
      while (connection_client.available()) {
        char c = connection_client.read();
        if (c == '\n') {
          if (getAll.length() == 0) {
            state = true;
          }
          getAll = "";
        }
        else if (c != '\r') {
          getAll += String(c);
        }
        if (state == true) {
          getBody += String(c);
        }
        startTimer = millis();
      }
      if (getBody.length() > 0) {
        break;
      }
    }
    Serial.println();
    connection_client.stop();
    Serial.println(getBody);

  }
  else
  {
    Serial.println("client level connection failure");
    //default setting
    getBody = "{'duty_cycle': 125,'flashlight': 1}";
  }

  return getBody;
}

template<class T1>
void send_image_form(T1 connection_client, camera_fb_t * fb)
{
  String getAll = "";
  String getBody = "";
  int timoutTimer = 20000;
  long startTimer = millis();
  boolean state = false;

  //form information
  String head_0 = "--esp32boundary\r\nContent-Disposition: form-data; name=\"imageFile\"; filename=\"esp32-temimage.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n";
  String tail_0 = "\r\n--esp32boundary\r\n";

  String head_1 = "--esp32boundary\r\nContent-Disposition: form-data; name=\"mac_address\"; \r\nContent-Type: text/plain\r\n\r\n";
  String content_1 = WiFi.macAddress();
  String tail_1 = "\r\n--esp32boundary\r\n";

  String head_2 = "--esp32boundary\r\nContent-Disposition: form-data; name=\"device_type\"; \r\nContent-Type: text/plain\r\n\r\n";
  String content_2 = "0";
  String tail_2 = "\r\n--esp32boundary--\r\n";

  //message length
  uint32_t content_0_length = fb->len;
  uint32_t totalLen = head_0.length() + content_0_length + tail_0.length() + head_1.length() + content_1.length() + tail_1.length() + head_2.length() + content_2.length() + tail_2.length();

  //ethernet has higher priority
  if (connection_client.connect(Host.c_str(), Port))
  {
    Serial.println("client level Connection successful!");
    connection_client.println("POST " + Path + " HTTP/1.1");
    connection_client.println("Host: " + Host);
    connection_client.println("Content-Length: " + String(totalLen));
    connection_client.println("Content-Type: multipart/form-data; boundary=esp32boundary");
    connection_client.println();
    connection_client.print(head_0);

    //send content of image file
    uint8_t *fbBuf = fb->buf;
    size_t fbLen = fb->len;
    for (size_t n = 0; n < fbLen; n = n + 1024) {
      if (n + 1024 < fbLen) {
        connection_client.write(fbBuf, 1024);
        fbBuf += 1024;
      }
      else if (fbLen % 1024 > 0) {
        size_t remainder = fbLen % 1024;
        connection_client.write(fbBuf, remainder);
      }
    }
    connection_client.print(tail_0);

    //send mac address
    connection_client.print(head_1);
    connection_client.print(content_1);
    connection_client.print(tail_1);

    //send device type
    connection_client.print(head_2);
    connection_client.print(content_2);
    connection_client.print(tail_2);

    while ((startTimer + timoutTimer) > millis()) {
      Serial.print(".");
      delay(100);
      while (connection_client.available()) {
        char c = connection_client.read();
        if (c == '\n') {
          if (getAll.length() == 0) {
            state = true;
          }
          getAll = "";
        }
        else if (c != '\r') {
          getAll += String(c);
        }
        if (state == true) {
          getBody += String(c);
        }
        startTimer = millis();
      }
      if (getBody.length() > 0) {
        break;
      }
    }
    Serial.println();
    connection_client.stop();
    Serial.println(getBody);

  }
  else
  {
    Serial.println("client level connection failure");
  }
}
# make a prediction for a new image.
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
single_digit_dir="../single_digit_images/"
# load and prepare the image
def load_image(filename):
	# load the image
	img = load_img(filename, grayscale=True, target_size=(28, 28))
	# convert to array
	img = img_to_array(img)
	# reshape into a single sample with 1 channel
	img = img.reshape(1, 28, 28, 1)
	# prepare pixel data
	img = img.astype('float32')
	img = img / 255.0
	return img
	
def decode_img(img):
  img= tf.io.read_file(img)
  img = tf.image.decode_png(img, channels=3) 
  img = tf.image.resize(img, [28, 28])
  img = (np.expand_dims(img,0))
  return img
  
def plot_value_array_owned(image,predictions_array,true_label):
  plt.subplot(1,2,1)
  plt.imshow(image)
  plt.subplot(1,2,2)
  plt.bar(range(20), predictions_array, tick_label = true_label, width = 0.8, color = ['red', 'green']) 
  plt.xlabel('x - axis')
  plt.ylabel('y - axis') 
  plt.title('weight') 
  plt.show()

# load an image and predict the class
def run_example():
	# load the image
	img = decode_img(single_digit_dir+'3289.png')
	img_n= tf.io.read_file(single_digit_dir+'3289.png')
	img_n= tf.io.decode_png(img_n,channels=3)
	# load model
	model = load_model('final_model_own.h5')
	# predict the class
	digit = model.predict(img)
	print(digit)
	class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9','1_0', '1_1', '1_2', '1_3', '1_4', '1_5', '1_6', '1_7', '1_8', '1_9']
	print(np.argmax(digit[0], axis=0))
	plot_value_array_owned(img_n,digit[0],class_names)
	

# entry point, run the example
run_example()

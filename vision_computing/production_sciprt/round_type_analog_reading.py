import cv2 
import numpy as np 
import os, sys
import matplotlib.pyplot as plt
from PIL import Image

im_original = cv2.imread('./images/meter10.jpeg')
hsv= cv2.cvtColor(im_original, cv2.COLOR_BGR2HSV)

# Threshold of blue in HSV space 
lower_red = np.array([30,150,50]) 
upper_red = np.array([255, 180, 180]) 

# preparing the mask to overlay 
mask = cv2.inRange(hsv, lower_red, upper_red) 

# The black region in the mask has the value of 0, 
# so when multiplied with original image removes all non-blue regions 
result = cv2.bitwise_and(im_original, im_original, mask = mask) 
gray= cv2.cvtColor(cv2.cvtColor(result, cv2.COLOR_HSV2RGB), cv2.COLOR_BGR2GRAY)
x_center,y_center=gray.shape
x_center=x_center/2
y_center=y_center/2
#edges = cv2.Canny(gray,50,150,apertureSize = 3)
lines = cv2.HoughLines(gray,1,np.pi/180, 10)
rho_max=0 
theta_hint=-1 
try:      
    for rho,theta in lines[0]:
        if rho>rho_max:
            rho_max=rho
            theta_hint=theta
    a = np.cos(theta_hint)
    b = np.sin(theta_hint)
    x0 = a*rho_max
    y0 = b*rho_max
    x1 = int(x0 + 1000*(-b))
    y1 = int(y0 + 1000*(a))
    x2 = int(x0 - 1000*(-b))
    y2 = int(y0 - 1000*(a))
    cv2.line(im_original,(x1,y1),(x2,y2),(0,0,255),2)
    if pow(abs(x1-x_center),2)+pow(abs(y1-y_center),2) > pow(abs(x2-x_center),2)+pow(abs(y2-y_center),2):
        if x1 <x_center:
            theta_hint=abs((theta_hint*180/np.pi))+180
        else:
            theta_hint=abs((theta_hint*180/np.pi))
    else:
        if x2 <x_center:
            theta_hint=abs((theta_hint*180/np.pi))+180
        else:
            theta_hint=abs((theta_hint*180/np.pi))
except TypeError:
    print("no")

finally:
    cv2.imshow("%3.0f"%theta_hint,im_original)
    cv2.waitKey(0)
    cv2.destroyAllWindows() 


import numpy as np
import cv2
import math
import os
import tflite_runtime.interpreter as tflite
import xlsxwriter
import time
import json

raw_image_dir="../raw_images/"
model_path="../final_model_lite_own.h5"
interpreter = tflite.Interpreter(model_path)
interpreter.allocate_tensors()
# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
            
def image_convert(img):
    img=cv2.resize(img, (28,28), interpolation = cv2.INTER_AREA)
    img = (np.expand_dims(img,0))
    return img.astype(np.float32)
    

def raw_image_to_circle_image_output(image,image_rotation):
    image=np.array(image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    need_rotation=1
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)
    height, width = image.shape[:2]
    distance_to_center=0
    minDist=0
    if height >width:
        minDist=width/2
        distance_to_center=height
    else:
        minDist=height/2
        distance_to_center=width

    # apply Canny edge detection using a wide threshold, tight
    # threshold, and automatically determined threshold
    wide = cv2.Canny(blurred, 10, 200)
    circles = cv2.HoughCircles(wide , cv2.HOUGH_GRADIENT, 1, minDist) 
    # ensure at least some circles were found
    max_r=0
    max_x=0
    max_y=0

    if circles is not None:
        # convert the (x, y) coordinates and radius of the circles to integers
        circles = np.round(circles[0, :]).astype("int")
        # loop over the (x, y) coordinates and radius of the circles
        # find the the least distance to center of the image
        for (x, y, r) in circles:
            # draw the circle in the output image, then draw a rectangle
            # corresponding to the center of the circle
            #cv2.circle(image, (x, y), r, (0, 255, 0), 4)
            #cv2.rectangle(image, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
            distance_to_center_tem=math.sqrt((x-width/2)*(x-width/2)+(y-height/2)*(y-height/2))
            if distance_to_center_tem<distance_to_center:
                max_r=r
                max_x=x
                max_y=y
                distance_to_center=distance_to_center_tem
                
        # find the small radius with 10 & torrloance distance 
        for (x, y, r) in circles:
            # draw the circle in the output image, then draw a rectangle
            # corresponding to the center of the circle
            #cv2.circle(image, (x, y), r, (0, 255, 0), 4)
            #cv2.rectangle(image, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
            distance_to_center_tem=math.sqrt((x-width/2)*(x-width/2)+(y-height/2)*(y-height/2))
            # 10% torrrolence
            if distance_to_center_tem<distance_to_center*1.1 and r<max_r:
                max_r=r
                max_x=x
                max_y=y
                distance_to_center=distance_to_center_tem
                
        # show the output image
        #cv2.circle(image, (max_x, max_y), max_r, (0, 255, 0), 4)
        image_cut=image[max_y-max_r:max_y+max_r,max_x-max_r:max_x+max_r]
        if image_rotation==90:
            image_cut=cv2.rotate(image_cut, cv2.ROTATE_90_CLOCKWISE)
        elif image_rotation==180:
            image_cut=cv2.rotate(image_cut, cv2.ROTATE_180)
        elif image_rotation==270:
            image_cut=cv2.rotate(image_cut, cv2.ROTATE_90_COUNTERCLOCKWISE)
            
        #print("-------------")
        #print(image.shape)
        #print(max_r,max_x,max_y)
        #print(image_cut.shape)

        # get pipeline and send the data to the pipe
        return image_cut
 

    else:
        return None

def raw_image_to_reading_integer_output(image,image_rotation,setting=None):
    if image is not None:
        image=raw_image_to_circle_image_output(image,image_rotation)
        if image is not None:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            height, width = gray.shape
            json_string=setting
            #default setting
            if setting is None:
                json_string="[[0.625,0.213,0.767,0.284],[0.625,0.307,0.767,0.378],[0.625,0.401,0.767,0.472],[0.625,0.495,0.767,0.566],[0.625,0.590,0.767,0.660],[0.625,0.684,0.767,0.755]]" 
            counter_area=json.loads(json_string)
            class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
            whole_number=""
            for i in counter_area:
                image=cv2.cvtColor(gray[int(height*i[0]):int(height*i[2]), int(width*i[1]):int(width*i[3])],cv2.COLOR_GRAY2RGB)
                image=image_convert(image)
                interpreter.set_tensor(input_details[0]['index'], image)
                interpreter.invoke()
                digit =interpreter.get_tensor(output_details[0]['index'])
                whole_number += class_names[np.argmax(digit[0], axis=0)]
            return whole_number
        
        else:
            return None
    
    else:
        return None

workbook = xlsxwriter.Workbook('tf_output_2.xlsx')
worksheet = workbook.add_worksheet()
row = 1
  
paths =os.listdir(raw_image_dir)
paths=sorted(paths)
number_of_success=0
index=0
start = time.time()
for file in paths:
    index +=1
    image = cv2.imread(os.path.join(raw_image_dir,file))
    whole_number=raw_image_to_reading_integer_output(image,180)
    print("-------------")
    print(os.path.join(raw_image_dir,file))
    print(whole_number)
    print("prcoess: ",index/len(paths))
    row += 1
end = time.time()
workbook.close()

print("the total time:  ")
print(end - start)
print("the number of images: ",len(paths))
    
    


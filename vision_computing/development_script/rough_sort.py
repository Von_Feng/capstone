#this sciprt is to sort any new image using pretrained model (old model)

import numpy as np
import cv2
import math
import os
import tflite_runtime.interpreter as tflite
import xlsxwriter
import time
import json

raw_image_dir="../raw_images/"
circle_image_dir="./circle_images/"
single_digit_dir="./single_digit_images"
model_path="../final_model_lite_own.h5"

def image_convert(img):
    img=cv2.resize(img, (28,28), interpolation = cv2.INTER_AREA)
    print(img.shape)
    img = (np.expand_dims(img,0))
    return img.astype(np.float32)
    

paths =os.listdir(single_digit_dir)
paths=sorted(paths)
index=0
start = time.time()
for file in paths:
    index +=1
    image =cv2.imread(os.path.join(single_digit_dir,file))
    resized_image=image_convert(image)

    interpreter = tflite.Interpreter(model_path)
    interpreter.allocate_tensors()
    # Get input and output tensors.
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    
    interpreter.set_tensor(input_details[0]['index'], resized_image)
    interpreter.invoke()
    digit =interpreter.get_tensor(output_details[0]['index'])
    cv2.imwrite(os.path.join("./tem_folder",class_names[np.argmax(digit[0], axis=0)],file),image)
    print(os.path.join("./tem_folder",class_names[np.argmax(digit[0], axis=0)],file))
    print("-------------")
    print(os.path.join(single_digit_dir,file))
    print("prcoess: ",index/len(paths))

print("the total time:  ")
print(time.time() - start)
print("the number of images: ",len(paths))
    
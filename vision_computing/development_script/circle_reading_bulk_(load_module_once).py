import numpy as np
import cv2
import math
import os
import tflite_runtime.interpreter as tflite
import xlsxwriter
import time
import json

circle_image_dir="./circle_images/"
model_path="../final_model_lite_own.h5"
interpreter = tflite.Interpreter(model_path)
interpreter.allocate_tensors()
# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
def image_convert(img):
    img=cv2.resize(img, (28,28), interpolation = cv2.INTER_AREA)
    img = (np.expand_dims(img,0))
    return img.astype(np.float32)
    

def circle_image_to_reading_integer_output(image,image_rotation,setting=None):
    if image is not None:
        if image is not None:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            height, width = gray.shape
            json_string=setting
            #default setting
            if setting is None:
                json_string="[[0.625,0.213,0.767,0.284],[0.625,0.307,0.767,0.378],[0.625,0.401,0.767,0.472],[0.625,0.495,0.767,0.566],[0.625,0.590,0.767,0.660],[0.625,0.684,0.767,0.755]]" 
            counter_area=json.loads(json_string)
 
            class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
            whole_number=""

            for i in counter_area:
                image=cv2.cvtColor(gray[int(height*i[0]):int(height*i[2]), int(width*i[1]):int(width*i[3])],cv2.COLOR_GRAY2RGB)
                image=image_convert(image)
                interpreter.set_tensor(input_details[0]['index'], image)
                interpreter.invoke()
                digit =interpreter.get_tensor(output_details[0]['index'])
                whole_number += class_names[np.argmax(digit[0], axis=0)]
            return whole_number
        
        else:
            return None
    
    else:
        return None

workbook = xlsxwriter.Workbook('tf_output.xlsx')
worksheet = workbook.add_worksheet()
row = 1
  
paths =os.listdir(circle_image_dir)
paths=sorted(paths)
number_of_success=0
index=0
start = time.time()
for file in paths:
    index +=1
    image = cv2.imread(os.path.join(circle_image_dir,file))
    whole_number=circle_image_to_reading_integer_output(image,180)
    print("-------------")
    print(os.path.join(circle_image_dir,file))
    print(whole_number)
    print("prcoess: ",index/len(paths))
    row += 1
end = time.time()
workbook.close()

print("the total time:  ")
print(end - start)
print("the number of images: ",len(paths))
    
    


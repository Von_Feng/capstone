import sys
import cv2 
import tensorflow as tf
import keras
print("-------------------------------------------------------")
print("python: ", sys.version)
print("opencv: ", cv2.__version__)
print("tensorflow: ", tf.__version__)
print("keras: ", keras.__version__)
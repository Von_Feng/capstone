from flask import Flask, request, Response,render_template
import base64
import json
import numpy as np
import cv2
import math
import io
import os
import tflite_runtime.interpreter as tflite
from matplotlib import pyplot as plt


default_setting="[[0.625,0.213,0.767,0.284],[0.625,0.307,0.767,0.378],[0.625,0.401,0.767,0.472],[0.625,0.495,0.767,0.566],[0.625,0.590,0.767,0.660],[0.625,0.684,0.767,0.755]]" 
model_path="model_lite_custom_dataset.h5"
model_rotation_path="model_lite_rotation_dataset.h5"
tol=0.3

def image_convert(img,width=28,height=28):
    img=cv2.resize(img, (width,height), interpolation = cv2.INTER_AREA)
    img=img[..., np.newaxis]
    img = (np.expand_dims(img,0))
    return img.astype(np.float32)
    
def raw_image_to_circle_image_output(image,image_rotation):
    image=np.array(image)
    x,y,channel=image.shape
    #target piexl=1024x768 and keep ratio of width and height of images
    x_ratio=x/768
    y_ratio=y/1024
    final_ratio=0
    if(x_ratio>y_ratio):
        final_ratio=y_ratio
    else:
        final_ratio=x_ratio   
    x=int(x/final_ratio)
    y=int(y/final_ratio)  

    image_resized=cv2.resize(image, (y,x), interpolation = cv2.INTER_AREA)
    image=image_resized
    gray = cv2.cvtColor(image_resized, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), cv2.BORDER_DEFAULT)
    height, width = image_resized.shape[:2]
    distance_to_center=0
    minDist=0
    if height >width:
        minDist=width/2
        distance_to_center=height
    else:
        minDist=height/2
        distance_to_center=width

    # apply Canny edge detection using a wide threshold
    # threshold, and automatically determined threshold
    wide = cv2.Canny(blurred, 10, 200)
    circles = cv2.HoughCircles(wide , cv2.HOUGH_GRADIENT, 1, minDist) 
    # ensure at least some circles were found
    max_r=0
    max_x=0
    max_y=0

    if circles is not None:
        # convert the (x, y) coordinates and radius of the circles to integers
        circles = np.round(circles[0, :]).astype("int")
        # loop over the (x, y) coordinates and radius of the circles
        # find the the least distance to center of the image
        
        for (x, y, r) in circles:
            distance_to_center_tem=math.sqrt((x-width/2)*(x-width/2)+(y-height/2)*(y-height/2))
            if distance_to_center_tem<distance_to_center:
                max_r=r
                max_x=x
                max_y=y
                distance_to_center=distance_to_center_tem
                
        # find the small radius with 10 & torrloance distance 
        for (x, y, r) in circles:
            distance_to_center_tem=math.sqrt((x-width/2)*(x-width/2)+(y-height/2)*(y-height/2))
            # 10% torrrolence
            if distance_to_center_tem<distance_to_center*1.1 and r<max_r:
                max_r=r
                max_x=x
                max_y=y
                distance_to_center=distance_to_center_tem

        #max_r=int(max_r*final_ratio)
        #max_x=int(max_x*final_ratio)  
        #max_y=int(max_y*final_ratio)
        # show the output image
        #cv2.circle(image, (max_x, max_y), max_r, (0, 255, 0), 4)
        image_cut=image[max_y-max_r:max_y+max_r,max_x-max_r:max_x+max_r]
        gray_cut=gray[max_y-max_r:max_y+max_r,max_x-max_r:max_x+max_r]
        if image_rotation<0:
            #using machine learing to chehck the rotation
            rotation_class=[0,90,180,270]
            interpreter = tflite.Interpreter(model_rotation_path)
            interpreter.allocate_tensors()
            # Get input and output tensors.
            input_details = interpreter.get_input_details()
            output_details = interpreter.get_output_details()
            gray_cut=image_convert(gray_cut,100,100)
            interpreter.set_tensor(input_details[0]['index'], gray_cut)
            interpreter.invoke()
            image_rotation=interpreter.get_tensor(output_details[0]['index'])
            image_rotation=rotation_class[np.argmax(image_rotation[0], axis=0)]
        if image_rotation==90:
            image_cut=cv2.rotate(image_cut, cv2.ROTATE_90_CLOCKWISE)
        elif image_rotation==180:
            image_cut=cv2.rotate(image_cut, cv2.ROTATE_180)
        elif image_rotation==270:
            image_cut=cv2.rotate(image_cut, cv2.ROTATE_90_COUNTERCLOCKWISE)  
        return image_cut
 

    else:
        if image_rotation==90:
            image=cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
        elif image_rotation==180:
            image=cv2.rotate(image, cv2.ROTATE_180)
        elif image_rotation==270:
            image=cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)  
        return image




def raw_image_to_circle_http_response(image,image_rotation):   
    image_cut=raw_image_to_circle_image_output(image,image_rotation)
    if image_cut is not None:
        plt.imshow(image_cut)
        plt.axis('off')
        pic_IObytes = io.BytesIO()
        plt.savefig(pic_IObytes,  format='png',bbox_inches='tight',pad_inches=0)
        pic_IObytes.seek(0)
        return Response(response=pic_IObytes.read(), status=200, mimetype="image/jpeg")

    else:
        return None

def raw_image_to_circle_http_response_base64(image,image_rotation):   
    image_cut=raw_image_to_circle_image_output(image,image_rotation)
    if image_cut is not None:
        plt.imshow(image_cut)
        plt.axis('off')
        pic_IObytes = io.BytesIO()
        plt.savefig(pic_IObytes,  format='png',bbox_inches='tight',pad_inches=0)
        pic_IObytes.seek(0)
        return Response(response= base64.b64encode(pic_IObytes.read()), status=200, mimetype="image/jpeg")

    else:
        return None







def raw_image_to_reading_integer_output(image,image_rotation,setting=None):
    if image is not None:
        image=raw_image_to_circle_image_output(image,image_rotation)
        if image is not None:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            height, width = gray.shape
            json_string=setting
            #default setting
            if setting is None:
                json_string=default_setting
            counter_area=json.loads(json_string)
            class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
            class_names=np.concatenate((class_names,class_names,class_names), axis=0)
            whole_number=""
            interpreter = tflite.Interpreter(model_path)
            interpreter.allocate_tensors()
            # Get input and output tensors.
            input_details = interpreter.get_input_details()
            output_details = interpreter.get_output_details()
            for i in counter_area:
                #defined scope
                image_0=gray[int(height*i[0]):int(height*i[2]), int(width*i[1]):int(width*i[3])]
                image_0=image_convert(image_0)
                interpreter.set_tensor(input_details[0]['index'], image_0)
                interpreter.invoke()
                digit_0 =interpreter.get_tensor(output_details[0]['index'])

                #move left scope and increase height
                image_1=gray[int(height*(i[0]-(i[2]-i[0])*tol/2)):int(height*(i[2]+(i[2]-i[0])*tol/2)), int(width*(i[1]-(i[3]-i[1])*tol)):int(width*(i[3]-(i[3]-i[1])*tol))]
                image_1=image_convert(image_1)
                interpreter.set_tensor(input_details[0]['index'], image_1)
                interpreter.invoke()
                digit_1 =interpreter.get_tensor(output_details[0]['index'])

                #move right scope and increase height
                image_2=gray[int(height*(i[0]-(i[2]-i[0])*tol/2)):int(height*(i[2]+(i[2]-i[0])*tol/2)), int(width*(i[1]+(i[3]-i[1])*tol)):int(width*(i[3]+(i[3]-i[1])*tol))]
                image_2=image_convert(image_2)
                interpreter.set_tensor(input_details[0]['index'], image_2)
                interpreter.invoke()
                digit_2 =interpreter.get_tensor(output_details[0]['index'])

                
                digit=np.concatenate((digit_0,digit_1,digit_2),axis=0)
                whole_number += class_names[np.argmax(digit[0], axis=0)]
            return whole_number,image
        
        else:
            return None,None
    
    else:
        return None,None

def most_recent_image_by_id(path):
    
    try:
        a_file=open(path, "rb") 
        return Response(response= base64.b64encode(a_file.read()), status=200, mimetype="image/jpeg")
    except Exception:
        return Response(response= "no image found 1", status=400, mimetype="plain/text")

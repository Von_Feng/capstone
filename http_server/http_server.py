from flask import Flask, request, Response,render_template,redirect,send_from_directory
import unity
import sql_unity
from PIL import Image
import os
import json
# Initialize the Flask application
app = Flask(__name__)


@app.route('/')
def root():
    return render_template('warnIng_system.html')

@app.route('/data_detail')
def data_detail():
    return render_template('data_detail.html')

@app.route('/reading_api')
def reading_api():
    return render_template('reading_api.html')
    
@app.route('/circle_api')
def circle_api():
    return render_template('circle_api.html')


@app.route('/test_device_input_api')
def test_device_input_api():
    return render_template('test_device_input_api.html')

@app.route('/new_device')
def new_device():
    return render_template('new_device.html')


@app.route('/registered_device')
def registered_device():
    return render_template('registered_device.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/images/<path:path>')
def images(path):
    print(path)
    return app.send_static_file(path)

#main function of the package

@app.route('/api/device_input', methods=['POST'])
def device_input():
    r = request
    mac_address=r.form['mac_address']
    device_type=r.form['device_type']
    device_type= int(device_type)
    #check if the device has an profile
    id=sql_unity.device_id(mac_address)
    if id != 0:        
        
        if device_type==0:
            location,rotation,setting=sql_unity.device_location_rotation_setting(id)
            file = r.files['imageFile']  
            image=Image.open(file.stream)
            if location!='no':
                
                value,circle_image=unity.raw_image_to_reading_integer_output(image,rotation,setting)
                #store the value
                value_index,value_location=sql_unity.new_value(id,value)
                #store the image
                if(os.path.isdir(os.path.join(app.root_path, 'static', 'circle_images', value_location))):
                    circle_image=Image.fromarray(circle_image)
                    circle_image.save(os.path.join(app.root_path, 'static', 'circle_images', value_location,str(value_index)+'.jpg'))
                else:
                    os.makedirs(os.path.join(app.root_path, 'static', 'circle_images', value_location))
                    circle_image=Image.fromarray(circle_image)
                    circle_image.save(os.path.join(app.root_path, 'static', 'circle_images', value_location,str(value_index)+'.jpg'))


            #update the most recent image
            image.save(os.path.join(app.root_path, 'static', 'latest_images', str(id)+'.jpg'))
            #file.save("./circle_images/"+str(id)+".jpg")
               
        else:
            location,rotation,setting=sql_unity.device_location_rotation_setting(id)
            if location!='no':               
                value=r.form['value']
                sql_unity.new_value(id,value,1)
        #update device check point
        sql_unity.device_checkpoint_update(id)
    else:
        sql_unity.new_device_profile(mac_address,device_type)
        id=sql_unity.device_id(mac_address)
        if device_type==0:
            
            file = r.files['imageFile']   
            #image=Image.open(file.stream)
            file.save(os.path.join(app.root_path, 'static', 'latest_images', str(id)+'.jpg'))
            #file.save("./circle_images/"+str(id)+".jpg")            
        sql_unity.device_checkpoint_update(id)
    return Response(response= "NO THING BAD HAPPENES", status=200, mimetype="text/html")

#main function of the package

@app.route('/api/device_flashlight_and_duty_circle', methods=['POST'])
def device_flashlight():
    r = request
    mac_address=r.form['mac_address']
    a_record={}
    a_record['flashlight']=sql_unity.device_flashlight_by_macaddress(mac_address)
    a_record['duty_cycle']=sql_unity.device_duty_cycle_by_macaddress(mac_address)
    json_obj = json.dumps(a_record, indent=4, sort_keys=True, default=str)
    return Response(response= json_obj, status=200, mimetype="application/json")


#return new device list in json format
@app.route('/new_device_registration_steps', methods=['GET'])
def new_device_registration_steps():
    id=request.args.get('id')
    device_type=request.args.get('device_type')
    try:
        id=int(id)
        device_type=int(device_type)
        if(device_type==0):
            return render_template('new_device_registration_steps_0.html',id=id, device_type=device_type,rotation=sql_unity.device_rotation(id),flashlight=sql_unity.device_flashlight(id),location=sql_unity.device_location(id),duty_cycle=sql_unity.device_duty_cycle(id))

        else:
            return render_template('new_device_registration_steps_1.html',id=id, device_type=device_type)       
    except Exception:
        return Response(response= "NO id number", status=400, mimetype="plain/text")


@app.route('/change_data_value', methods=['GET'])
def change_data_value_html():
    index=request.args.get('index')
    image_path=request.args.get('image_path')
    return render_template('change_data_value.html',index=index, image_path=image_path)

#return new image file
@app.route('/api/recent_image', methods=['GET'])
def most_recent_image_by_id():
    id=request.args.get('id')
    try:
        int(id)
        return unity.most_recent_image_by_id(os.path.join(app.root_path, 'static', 'latest_images', str(id)+'.jpg'))
    except Exception:
        return Response(response= "NO id number 0", status=400, mimetype="plain/text")


#return circle in png format
@app.route('/api/circle', methods=['POST'])
def raw_image_to_circle_http_response():
    r = request
    # convert string of image data to uint8
    file = r.files['imageFile']   
    image=Image.open(file.stream)
    image_rotation=int(r.form['image_rotation'])
    return unity.raw_image_to_circle_http_response(image,image_rotation)

#return circle_base64
@app.route('/api/circle_base64', methods=['POST'])
def raw_image_to_circle_http_response_base64():
    r = request
    # convert string of image data to uint8
    file = r.files['imageFile']
    image=Image.open(file.stream)
    image_rotation=int(r.form['image_rotation'])
    return unity.raw_image_to_circle_http_response_base64(image,image_rotation)


#return reading
@app.route('/api/reading', methods=['POST'])
def raw_image_to_reading_http_response():
    r = request
    # convert string of image data to uint8
    file = r.files['imageFile']   
    image=Image.open(file.stream)
    image_rotation=int(r.form['image_rotation'])
    value,circle_image=unity.raw_image_to_reading_integer_output(image,image_rotation)
    return value


#return new device rotation in plain text format
@app.route('/api/device/rotation', methods=['GET'])
def device_rotation():
    id=request.args.get('id')
    try:
        int(id)
        return Response(response= str(sql_unity.device_rotation(id)), status=200, mimetype="application/json")
    except Exception:
        return Response(response= "NO id number", status=400, mimetype="plain/text")

#return no ouput
@app.route('/api/device/change_device_profile', methods=['GET'])
def change_device_profile():
    id=request.args.get('id')
    device_type=request.args.get('device_type')
    location=request.args.get('location')
    try:
        id= int(id)
        device_type=int(device_type)
        if device_type==0:
           
            try:
                setting=request.args.get('setting')
            except Exception:
                setting=None
            
            try:
                lower_bound_value=request.args.get('lower_bound_value')
                lower_bound_value=int(lower_bound_value)
            except Exception:
                lower_bound_value=0
            try:  
                rotation=request.args.get('rotation')
                rotation=int(rotation)
            except Exception:
                rotation=0
            try:
                flashlight=request.args.get('flashlight')
                flashlight=int(flashlight)
            except Exception:
                flashlight=0

            try:
                duty_cycle=request.args.get('duty_cycle')
                duty_cycle=int(duty_cycle)
            except Exception:
                duty_cycle=125
            print(duty_cycle)       
            sql_unity.device_profile_update(id,location,setting,rotation,flashlight,duty_cycle)
            if location!='no':
            	sql_unity.new_value(id,lower_bound_value,1)
        elif device_type==1:
            sql_unity.device_profile_update(id,location)
        return redirect("../../registered_device", code=302)
    except Exception as error:
        print('Caught this error: ' + repr(error))
        return Response(response= "NO id number", status=400, mimetype="plain/text")


#return new device rotation in plain text format
@app.route('/api/device/setting', methods=['GET'])
def device_setting():
    id=request.args.get('id')
    try:
        int(id)
        return Response(response= str(sql_unity.device_setting(id)), status=200, mimetype="application/json")
    except Exception:
        return Response(response= "NO id number", status=400, mimetype="plain/text")


#return new device list in json format
@app.route('/api/new_device_list', methods=['GET'])
def new_device_list():
    return sql_unity.new_device_list()

#return registered device list in json format
@app.route('/api/registered_device_list', methods=['GET'])
def registered_device_list():
    return sql_unity.registered_device_list()


#return value list in json format
@app.route('/api/value_list', methods=['GET'])
def value_list():
    location=None
    start_time=None
    end_time=None
    number_of_values=None
    try:
        location=request.args.get('location')
        if location=='':
            location=None
    except Exception:
        location=None
    try:
        number_of_values=request.args.get('number_of_values')
        number_of_values=int(number_of_values)
    except Exception:
        number_of_values=None   
    try:
        start_time=request.args.get('start_time')
        if start_time=='':
            start_time=None
    except Exception:
        start_time=None
    try:
        end_time=request.args.get('end_time')
        if end_time=='':
            end_time=None
    except Exception:
        end_time=None
    return sql_unity.value_list(location,start_time,end_time,number_of_values)

#return location device list in json format
@app.route('/api/location_list', methods=['GET'])
def location_list():
    return sql_unity.location_list()

#return location device list in json format
@app.route('/api/value_list_graph', methods=['GET'])
def value_list_graph():
    return sql_unity.value_list_graph()

#return location device list in json format
@app.route('/api/value_list_graph_2', methods=['GET'])
def value_list_graph2():
    ratio=0
    try:
        ratio=request.args.get('ratio')
        ratio=float(ratio)
    except Exception:
        ratio=0  
    return sql_unity.value_list_graph2(ratio)

#return location device list in json format
@app.route('/api/summary_list_graph', methods=['GET'])
def summary_list_graph():
    return sql_unity.summary_list_graph()

#return csv file
@app.route('/api/csv', methods=['GET'])
def csv():
    ratio=0
    try:
        ratio=request.args.get('ratio')
        ratio=float(ratio)
    except Exception:
        ratio=0
    return sql_unity.csv_file(ratio)


#
@app.route('/api/change_data_value', methods=['GET'])
def change_data_value():
    index=request.args.get('index')
    value=request.args.get('data_value')
    sql_unity.change_data_value(index,value)
    return redirect("../data_detail", code=302)

# start flask app
if __name__ == "__main__":
    app.run(host='0.0.0.0')
